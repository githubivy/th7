import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  State<StatefulWidget> createState(){
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<StatefulWidget>{
  late LatLng userPos;
  List<Marker> markers = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('My Map'),),
        body: FutureBuilder(
          future: findUserLocation(),
          builder: (BuildContext context, AsyncSnapshot snap){
            if(snap.hasData)
            {
              return GoogleMap(
                initialCameraPosition: CameraPosition(
                    target: snap.data,zoom:15
                ),
                markers: Set<Marker>.of(markers),
              );
            }
            else{
              return setContentLoading();
            }
          },
        )
    );
  }



  Widget setContentLoading(){
    return Align(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Please wait',style: const TextStyle(fontSize: 25, color: Colors.blueAccent),),
          SizedBox(height: 20),
          CircularProgressIndicator()
        ],
      ),
    );
  }

  Future<LatLng> findUserLocation() async {
    Location location = Location();
    LocationData userLocation;
    PermissionStatus hasPermission = await location.hasPermission();
    bool active = await location.serviceEnabled();
    if(hasPermission == PermissionStatus.granted && active){
      userLocation = await location.getLocation();

      userPos = LatLng(userLocation.latitude as double,userLocation.longitude as double);
    }
    else{
      userPos = LatLng(10.732869174213993, 106.69973741130023 );
    }
    if(markers.isEmpty){
      markers.add(buildMarker(userPos));
    }
    else{
      markers[0] = buildMarker(userPos);
    }

    setState(() {});
    return userPos;
  }

  Marker buildMarker(LatLng pos){
    MarkerId markerID = MarkerId('H');
    Marker marker = Marker(
        markerId: markerID,
        position: pos
    );
    return marker;
  }


}